##
## EPITECH PROJECT, 2017
## My ls
## File description:
## Makefile
##

SRC_DIR	=	$(realpath src)

CFLAGS	=	-Wall -Wextra -Iinclude/ -L lib/ -lmy

SRC	=	$(SRC_DIR)/check_space.c	\
		$(SRC_DIR)/choose_option.c	\
		$(SRC_DIR)/display_list.c	\
		$(SRC_DIR)/full_struct.c	\
		$(SRC_DIR)/get_data.c		\
		$(SRC_DIR)/get_permissions.c	\
		$(SRC_DIR)/manage_dir.c		\
		$(SRC_DIR)/memory_tab.c		\
		$(SRC_DIR)/my_ls.c		\
		$(SRC_DIR)/sort_list.c		\
		$(SRC_DIR)/sort_option.c	\
		$(SRC_DIR)/useful_function.c	\
		$(SRC_DIR)/manage_file.c	\
		$(SRC_DIR)/display_texte.c

OBJ	=	$(SRC:.c=.o)

NAME	=	my_ls

all:            $(NAME)

$(NAME):	$(OBJ)
		make -C lib/my/
		gcc -g -o $(NAME) $(OBJ) $(CFLAGS)

clean:
		rm -f $(OBJ)
		make clean -C lib/my/

fclean:		clean
		make fclean -C lib/my/
		rm -f $(NAME)

re:		fclean all

test_run:
		make -C tests/
		./tests/unit-tests
		make fclean -C tests/
