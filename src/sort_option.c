/*
** EPITECH PROJECT, 2017
** My ls
** File description:
** choose and sort option
*/

#include <sys/stat.h>
#include <sys/types.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <dirent.h>
#include <pwd.h>
#include <grp.h>
#include <errno.h>
#include <time.h>
#include "my.h"
#include "data.h"

char	*check_option(char *option, char new_option)
{
	int	count = 0;
	int	opt = 0;
	char	*check = "dlRtr";
	char	*option2 = malloc(sizeof(char) * (my_strlen(option) + 1));

	while (check[count] != 0){
		if (check[count] == new_option){
			option2[count] = check[count];
			opt++;
		} else
			option2[count] = option[count];
		count++;
	} if (opt == 0){
		write(2, "ls: invalid option -- ", 22);
		write(2, &new_option, 1);
		my_putchar('\n');
		exit(84);
	}
	option2[count] = 0;
	return option2;
}

char	*add_option(char *new_option, char *option)
{
	int	count = 1;

	while (new_option[count] != 0) {
		option = check_option(option, new_option[count]);
		count++;
	}
	return option;
}
