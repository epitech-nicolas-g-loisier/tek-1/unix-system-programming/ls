/*
* EPITECH PROJECT, 2017
** My ls
** File description:
** get permissions of files
*/

#include <sys/stat.h>
#include <sys/types.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <dirent.h>
#include <pwd.h>
#include <grp.h>
#include <errno.h>
#include <time.h>
#include "my.h"
#include "data.h"

char	*get_user_perm(struct stat stat_file, char *perms)
{
	perms[1] = (stat_file.st_mode & S_IRUSR) ? 'r' : '-';
	perms[2] = (stat_file.st_mode & S_IWUSR) ? 'w' : '-';
	perms[3] = (stat_file.st_mode & S_IXUSR) ? 'x' : '-';
	return (perms);
}

char	*get_grp_perm(struct stat stat_file, char *perms)
{
	perms[4] = (stat_file.st_mode & S_IRGRP) ? 'r' : '-';
	perms[5] = (stat_file.st_mode & S_IWGRP) ? 'w' : '-';
	perms[6] = (stat_file.st_mode & S_IXGRP) ? 'x' : '-';
	return (perms);
}

char	*get_other_perm(struct stat stat_file, char *perms)
{
	perms[7] = (stat_file.st_mode & S_IROTH) ? 'r' : '-';
	perms[8] = (stat_file.st_mode & S_IWOTH) ? 'w' : '-';
	perms[9] = (stat_file.st_mode & S_IXOTH) ? 'x' : '-';
	return (perms);
}

char	*get_permissions(struct stat stat_file)
{
	char	*perms = malloc(sizeof(char) * (12));

	perms[0] = (S_ISDIR(stat_file.st_mode)) ? 'd' : '-';
	perms = get_user_perm(stat_file, perms);
	perms = get_grp_perm(stat_file, perms);
	perms = get_other_perm(stat_file, perms);
	perms[10] = 0;
	return perms;
}
