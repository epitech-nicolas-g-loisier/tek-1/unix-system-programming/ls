/*
** EPITECH PROJECT, 2017
** My ls
** File description:
** Manage dir and file
*/

#include <sys/stat.h>
#include <sys/types.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <dirent.h>
#include <pwd.h>
#include <grp.h>
#include <errno.h>
#include <time.h>
#include "my.h"
#include "data.h"

char	**sort_dir(char **dir)
{
	int	i = 0;
	char	*str = NULL;

	while (dir[(i + 1)] != NULL){
		if (my_strcmp(dir[i], dir[(i + 1)]) > 0){
			str = dir[i];
			dir[i] = dir[(i + 1)];
			dir[(i + 1)] = str;
			i = -1;
		}
		i++;
	}
	return (dir);
}

int	check_if_exist(char *dir)
{
	struct	stat	stat_file;

	if (stat(dir, &stat_file) != -1 && dir[0] != '*'){
		return 0;
	} else {
		write(2, "ls: cannot access '", 19);
		write(2, dir, my_strlen(dir));
		write(2, "': No such file or directory\n", 29);
		return 84;
	}
}

char	**exit_file(char **dir, char *opt)
{
	int	i = 0;
	int	j = 0;
	int	count = 0;
	char	**new_dir = malloc_tab_char(dir);
	char	**file = malloc_tab_char(dir);
	struct	stat	stat_file;

	if (opt[0] == 'd'){
		display_info_file(dir, opt);
		exit(0);
	} while (dir[i] != NULL){
		stat(dir[i], &stat_file);
		if (S_ISDIR(stat_file.st_mode) != 1){
			file[count] = dir[i];
			count++;
		} else {
			new_dir[j] = dir[i];
			j++;
		}
		i++;
	}
	new_dir[j] = NULL;
	file[count] = NULL;
	display_info_file(file, opt);
	if (new_dir[0] != NULL && file[0] != NULL)
		my_putchar('\n');
	free(dir);
	return (new_dir);
}

void	choose_display_dir(char **dir, char *opt)
{
	char	**new_dir = NULL;

	new_dir	= exit_file(sort_dir(dir), opt);
	if (opt[0] == 'd'){
		display_info_file(new_dir, opt);
	}
	if (new_dir[1] == NULL && new_dir[0] != NULL && opt[0] != 'd'){
		if (opt[1] == 'l')
			full_data(new_dir[0], opt);
		else
			full_name(new_dir[0], opt);
	} else if (new_dir[0] != NULL && opt[0] != 'd'){
		choose_dir(new_dir, opt);
	}
	free(new_dir);
}

void	choose_dir(char **dir, char *option)
{
	int	i = 0;
	struct	stat	stat_file;

	while (dir[i] != NULL){
		stat(dir[i], &stat_file);
		if ((stat_file.st_mode & S_IROTH) != 0){
			full_list(dir[i], option);
		} else {
			write(2, "ls: cannot open directory '", 27);
			write(2, dir[i], my_strlen(dir[i]));
			write(2, "': Permission denied\n", 21);
		}
		i++;
		stat(dir[i], &stat_file);
		if (dir[i] != NULL && (stat_file.st_mode & S_IROTH) != 0)
			my_putchar('\n');
	}
}
