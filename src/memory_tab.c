/*
** EPITECH PROJECT, 2017
** My ls
** File description:
** free tab
*/

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include "my.h"

void	free_tab_char(char **tab)
{
	int	i = 0;

	while (tab[i] != NULL){
		free(tab[i]);
		i++;
	}
	free(tab);
}

char	**malloc_tab_char(char **tab)
{
	int	count = 0;
	int	lenght = 0;
	char	**new_tab = NULL;

	while (tab[count] != NULL){
		if (my_strlen(tab[count]) > lenght)
			lenght = my_strlen(tab[count]);
		count++;
	}
	new_tab = malloc(sizeof(char*) * (count + 1));
	new_tab[count] = NULL;
	count--;
	while (count >= 0){
		new_tab[count] = malloc(sizeof(char) * (lenght + 1));
		count--;
	}
	return (new_tab);
}
