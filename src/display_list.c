/*
** EPITECH PROJECT, 2017
** My ls
** File description:
** display ls result
*/

#include <sys/stat.h>
#include <sys/types.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <dirent.h>
#include <pwd.h>
#include <grp.h>
#include <errno.h>
#include <time.h>
#include "my.h"
#include "data.h"

char	*find_name(char *path)
{
	int	count = 0;
	int	slash = 0;
	char	*name = NULL;

	while (path[count] != 0){
		if (path[count] == '/' && path[count + 1] != 0)
			slash = count;
		count++;
	}
	if (path[slash] == '/')
		slash++;
	name = &path[slash];
	return (name);
}

int	rev_display_list_name(data_t* list)
{
	data_t*	tmp_list = list->prev;

	while (tmp_list != list){
		display_name(tmp_list);
		tmp_list = tmp_list->prev;
	}
	remove_name_list(list);
	return (0);
}

int	rev_display_list_data(data_t* list)
{
	data_t*	tmp_list = list->prev;

	check_space(list);
	while (tmp_list != list){
		display_full_data(tmp_list);
		tmp_list = tmp_list->prev;
	}
	remove_full_list(list);
	return (0);
}

int	display_list_name(data_t* list)
{
	data_t*	tmp_list = list->next;

	while (tmp_list != list){
		display_name(tmp_list);
		tmp_list = tmp_list->next;
	}
	remove_name_list(list);
	return (0);
}

int	display_list_data(data_t* list)
{
	data_t*	tmp_list = list->next;

	check_space(list);
	while (tmp_list != list){
		display_full_data(tmp_list);
		tmp_list = tmp_list->next;
	}
	remove_full_list(list);
	return (0);
}
