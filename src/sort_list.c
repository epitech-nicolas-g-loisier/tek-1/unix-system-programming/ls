/*
** EPITECH PROJECT, 2017
** My ls
** File description:
** sort the list
*/

#include <sys/stat.h>
#include <sys/types.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <dirent.h>
#include <pwd.h>
#include <grp.h>
#include <errno.h>
#include <time.h>
#include "my.h"
#include "data.h"

void	swap_data(data_t *one, data_t *two)
{
	data_t	*tmp1 = one->prev;
	data_t	*tmp2 = two->next;

	one->next = tmp2;
	tmp2->prev = one;
	two->next = one;
	one->prev = two;
	tmp1->next = two;
	two->prev = tmp1;
}

void	ascii_order(data_t *data)
{
	data_t *tmp = data->next;

	while (tmp->next != data){
		if (my_strcmp(tmp->name, tmp->next->name) > 0){
			swap_data(tmp, tmp->next);
			tmp = data;
		}
		tmp = tmp->next;
	}
}

void	chrono_order(data_t *data)
{
	data_t *tmp = data->next;

	while (tmp->next != data){
		if (tmp->time < tmp->next->time){
			swap_data(tmp, tmp->next);
			tmp = data;
		}
		tmp = tmp->next;
	}
}
