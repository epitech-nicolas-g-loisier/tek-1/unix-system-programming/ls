/*
** EPITECH PROJECT, 2017
** My ls
** File description:
** Useful function for chained list
*/

#include <sys/stat.h>
#include <sys/types.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <dirent.h>
#include <pwd.h>
#include <grp.h>
#include <errno.h>
#include <time.h>
#include "my.h"
#include "data.h"

data_t*	creat_root(void)
{
	data_t* root = malloc(sizeof(data_t));

	if (root == NULL)
		return (NULL);
	root->block_size = 0;
	root->prev = root;
	root->next = root;
	return (root);
}

int	remove_full_list(data_t* list)
{
	data_t* tmp = list->next;

	while (tmp != list){
		tmp->prev->next = tmp->next;
		tmp->next->prev = tmp->prev;
		free(tmp->name);
		free(tmp->date);
		free(tmp->permissions);
		free(tmp->link);
		free(tmp->size);
		free(tmp->owner);
		free(tmp->group);
		free(tmp->path);
		free(tmp);
		tmp = list->next;
	}
	free(list);
	return (0);

}

int	remove_name_list(data_t* list)
{
	data_t* tmp = list->next;

	while (tmp != list){
		tmp->prev->next = tmp->next;
		tmp->next->prev = tmp->prev;
		free(tmp->name);
		free(tmp->permissions);
		free(tmp->path);
		free(tmp);
		tmp = list->next;
	}
	free(list);
	return (0);
}

void	add_to_list_name(data_t* list, struct dirent *entry, char *directory)
{
	data_t*	new_element = malloc(sizeof(data_t));
	char	*path = find_path(entry->d_name, directory);
	struct	stat	stat_file;

	stat(path, &stat_file);
	new_element->name = my_strdup(entry->d_name);
	new_element->permissions = get_permissions(stat_file);
	new_element->time = stat_file.st_mtime;
	new_element->path = path;
	new_element->prev = list->next;
	new_element->next = list;
	list->prev->next = new_element;
	list->prev = new_element;
}

void	add_to_list_data(data_t* list, struct dirent *entry, char *directory)
{
	data_t*	new_element = malloc(sizeof(data_t));
	char	*path = find_path(entry->d_name, directory);
	struct	stat	stat_file;

	stat(path, &stat_file);
	list->block_size += stat_file.st_blocks;
	new_element->name = my_strdup(entry->d_name);
	new_element->permissions = get_permissions(stat_file);
	new_element->link = long_to_str(stat_file.st_nlink);
	new_element->owner = get_uid(stat_file);
	new_element->group = get_gid(stat_file);
	new_element->size = long_to_str(stat_file.st_size);
	find_date(ctime(&stat_file.st_mtime), new_element);
	new_element->time = stat_file.st_mtime;
	new_element->path = path;
	new_element->prev = list->next;
	new_element->next = list;
	list->prev->next = new_element;
	list->prev = new_element;
}
