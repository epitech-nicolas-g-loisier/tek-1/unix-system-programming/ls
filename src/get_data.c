/*
** EPITECH PROJECT, 2017
** My ls
** File description:
** get data from file
*/

#include <sys/stat.h>
#include <sys/types.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <dirent.h>
#include <pwd.h>
#include <grp.h>
#include <errno.h>
#include <time.h>
#include "my.h"
#include "data.h"

char	*get_uid(struct stat stat_file)
{
	int	i = 0;
	char	*uid = NULL;
	struct	passwd	*pwd = malloc(sizeof(struct passwd*));

	if (pwd == NULL)
		exit(84);
	pwd = getpwuid(stat_file.st_uid);
	uid = malloc(sizeof(char) * my_strlen(pwd->pw_name) + 1);
	while (pwd->pw_name[i] != 0){
		uid[i] = pwd->pw_name[i];
		i++;
	}
	uid[i] = 0;
	return (uid);
}

char	*get_gid(struct stat stat_file)
{
	int	i = 0;
	char	*gid = NULL;
	struct	group	*grp = malloc(sizeof(struct passwd*));

	if (grp == NULL)
		exit(84);
	grp = getgrgid(stat_file.st_uid);
	gid = malloc(sizeof(char) * my_strlen(grp->gr_name) + 1);
	while (grp->gr_name[i] != 0){
		gid[i] = grp->gr_name[i];
		i++;
	}
	gid[i] = 0;
	return (gid);
}

void	find_date(char *old_date, data_t *element)
{
	int	counter = 0;
	int	count = 4;

	element->date = malloc(sizeof(char) * 13);
	while (count < 16){
		element->date[counter] = old_date[count];
		count++;
		counter++;
	}
	element->date[counter] = 0;
	element->year = &old_date[20];
}

char	*find_path(char *file_name, char *directory)
{
	int	count = 0;
	int	counter = 0;
	int	lenght = my_strlen(directory) + my_strlen(file_name);
	char	*path = malloc(sizeof(char) * (lenght + 2));

	while (directory[count] != 0){
		path[count] = directory[count];
		count++;
	} if (path[count - 1] != '/'){
		path[count] = '/';
		count++;
	} while (file_name[counter] != 0){
		path[count] = file_name[counter];
		count++;
		counter++;
	}
	path[count] = 0;
	return path;
}
