/*
** EPITECH PROJECT, 2017
** My ls
** File description:
** Check space to make columns
*/

#include <sys/stat.h>
#include <sys/types.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <dirent.h>
#include <pwd.h>
#include <grp.h>
#include <errno.h>
#include <time.h>
#include "my.h"
#include "data.h"

char	*full_space_right(int lenght, char *str)
{
	int	count = 0;
	char	*new_str = malloc(sizeof(char) * lenght + 1);

	if (new_str == NULL)
		exit(84);
	while (str[count] != 0){
		new_str[count] = str[count];
		count++;
	} while (count < lenght){
		new_str[count] = ' ';
		count++;
	}
	new_str[count] = 0;;
	return (new_str);
}

char	*full_space(int lenght, char *str)
{
	int	count = 0;
	int	counter = 0;
	char	*new_str = malloc(sizeof(char) * lenght + 1);

	if (new_str == NULL)
		exit(84);
	while (lenght > my_strlen(str)){
		new_str[count] = ' ';
		count++;
		lenght--;
	} while (str[counter] != 0){
		new_str[count] = str[counter];
		count++;
		counter++;
	}
	new_str[count] = 0;;
	return (new_str);
}

void	replace_data(data_t *data, int *lenght)
{
	data_t	*tmp = data->next;

	while (tmp != data){
		tmp->link = full_space(lenght[0], tmp->link);
		tmp->owner = full_space_right(lenght[1], tmp->owner);
		tmp->group = full_space_right(lenght[2], tmp->group);
		tmp->size = full_space(lenght[3], tmp->size);
		tmp = tmp->next;
	}
}

void	check_space(data_t *data)
{
	int	lenght[4] = {0, 0, 0, 0};
	data_t	*tmp = data->next;

	while (tmp != data){
		if (my_strlen(tmp->link) > lenght[0])
			lenght[0] = my_strlen(tmp->link);
		if (my_strlen(tmp->owner) > lenght[1])
			lenght[1] = my_strlen(tmp->owner);
		if (my_strlen(tmp->group) > lenght[2])
			lenght[2] = my_strlen(tmp->group);
		if (my_strlen(tmp->size) > lenght[3])
			lenght[3] = my_strlen(tmp->size);
		tmp = tmp->next;
	}
	replace_data(data, lenght);
}
