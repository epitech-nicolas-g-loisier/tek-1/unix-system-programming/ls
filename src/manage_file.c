/*
** EPITECH PROJECT, 2017
** My ls
** File description:
** manage file data
*/

#include <sys/stat.h>
#include <sys/types.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <dirent.h>
#include <pwd.h>
#include <grp.h>
#include <errno.h>
#include <time.h>
#include "my.h"
#include "data.h"

void	add_data_file(char* file, data_t *list)
{
	data_t*	new_element = malloc(sizeof(data_t));
	char	*path = my_strdup(file);
	struct	stat	stat_file;

	stat(path, &stat_file);
	list->block_size += stat_file.st_blocks;
	new_element->name = my_strdup(file);
	new_element->permissions = get_permissions(stat_file);
	new_element->link = long_to_str(stat_file.st_nlink);
	new_element->owner = get_uid(stat_file);
	new_element->group = get_gid(stat_file);
	new_element->size = long_to_str(stat_file.st_size);
	find_date(ctime(&stat_file.st_mtime), new_element);
	new_element->time = stat_file.st_mtime;
	new_element->path = path;
	new_element->prev = list;
	new_element->next = list->next;
	list->next->prev = new_element;
	list->next = new_element;
}

void	add_name_file(char* file, data_t *list)
{
	data_t*	new_element = malloc(sizeof(data_t));
	char	*path = my_strdup(file);
	struct	stat	stat_file;

	stat(path, &stat_file);
	new_element->name = my_strdup(file);
	new_element->permissions = get_permissions(stat_file);
	new_element->time = stat_file.st_mtime;
	new_element->path = path;
	new_element->prev = list;
	new_element->next = list->next;
	list->next->prev = new_element;
	list->next = new_element;
}

void	display_info_file(char **file, char *option)
{
	int	count = 0;
	data_t	*element = creat_root();

	while (file[count] != NULL){
		if (option[1] == 'l')
			add_data_file(file[count], element);
		else
			add_name_file(file[count], element);
		count++;
	}
	if (option[1] == 'l')
		choose_full_display_option(element, option);
	else
		choose_name_display_option(element, option);
}
