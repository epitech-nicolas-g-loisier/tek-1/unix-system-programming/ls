/*
** EPITECH PROJECT, 2017
** My ls
** File description:
** Display information
*/

#include <sys/stat.h>
#include <sys/types.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <dirent.h>
#include <pwd.h>
#include <grp.h>
#include <errno.h>
#include <time.h>
#include "my.h"
#include "data.h"

int	main(int argc, char **argv)
{
	int	i = 0;
	int	count = 1;
	char	*option = "-----";
	char	**dir = malloc(sizeof(char*) * (argc + 1));

	dir[0] = ".";
	while (count < argc){
		if (argv[count][0] == '-')
			option = add_option(argv[count], option);
		else {
			if (check_if_exist(argv[count]) == 0){
				dir[i] = argv[count];
				i++;
			} else
				dir[i] = NULL;
		}
		count++;
	}
	if (dir[0][0] == '.')
		dir[i + 1] = NULL;
	else
		dir[i] = NULL;
	choose_display_dir(dir, option);
	return 0;
}
