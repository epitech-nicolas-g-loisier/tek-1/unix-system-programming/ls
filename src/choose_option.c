/*
** EPITECH PROJECT, 2017
** My ls
** File description:
** choose option of display
*/

#include <dirent.h>
#include <stdlib.h>
#include <errno.h>
#include "my.h"
#include "data.h"

void	choose_name_display_option(data_t *data, char* option)
{
	if (option[3] == 't'){
		chrono_order(data);
	}if (option[4] == 'r'){
		rev_display_list_name(data);
	} else {
		display_list_name(data);
	}
}

void	choose_full_display_option(data_t *data, char* option)
{
	if (option[3] == 't')
		chrono_order(data);
	if (option[4] == 'r')
		rev_display_list_data(data);
	else
		display_list_data(data);
}
