/*
** EPITECH PROJECT, 2017
** My ls
** File description:
** full the structure
*/

#include <sys/stat.h>
#include <sys/types.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <dirent.h>
#include <pwd.h>
#include <grp.h>
#include <errno.h>
#include <time.h>
#include "my.h"
#include "data.h"

void	full_name(char *directory, char *option)
{
	DIR	*dir = NULL;
	struct	dirent	*entry = NULL;
	struct	stat	stat_file;
	data_t	*data = creat_root();

	stat(directory, &stat_file);
	dir = opendir(directory);
	while ((entry = readdir(dir)) != NULL){
		if (entry->d_name[0] != '.'){
			add_to_list_name(data, entry, directory);
		}
	}
	closedir(dir);
	choose_name_display_option(data, option);
}

void	full_data(char *directory, char *option)
{
	DIR	*dir = NULL;
	struct	dirent	*entry = NULL;
	struct	stat	stat_file;
	data_t	*data = creat_root();

	dir = opendir(directory);
	stat(directory, &stat_file);
	while ((entry = readdir(dir)) != NULL){
		if (entry->d_name[0] != '.'){
			add_to_list_data(data, entry, directory);
		}
	}
	closedir(dir);
	my_printf("total %d\n", data->block_size / 2);
	choose_full_display_option(data, option);
}

void	full_list(char *dir, char *option)
{
	my_printf("%s:\n", dir);
	if (option[1] == 'l')
		full_data(dir, option);
	else
		full_name(dir, option);
}
