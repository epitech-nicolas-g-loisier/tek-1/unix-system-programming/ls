/*
** EPITECH PROJECT, 2018
** My Ls
** File description:
** display ls info
*/

#include <stdlib.h>
#include "data.h"

void	display_full_data(data_t *list)
{
	write(1, list->permissions, my_strlen(list->permissions));
	write(1, " ", 1);
	write(1, list->link, my_strlen(list->link));
	write(1, " ", 1);
	write(1, list->owner, my_strlen(list->owner));
	write(1, " ", 1);
	write(1, list->group, my_strlen(list->group));
	write(1, " ", 1);
	write(1, list->size, my_strlen(list->size));
	write(1, " ", 1);
	write(1, list->date, my_strlen(list->date));
	write(1, " ", 1);
	write(1, find_name(list->path),
		my_strlen(find_name(list->path)));
	write(1, "\n", 1);
}

void	display_name(data_t *list)
{
	write(1, find_name(list->path),
		my_strlen(find_name(list->path)));
	write(1, "\n", 1);
}
