/*
** EPITECH PROJECT, 2017
** my_strcmp
** File description:
** compare two strings in alphabetic order
*/

#include <stdlib.h>
#include <stdio.h>
#include "my.h"

int my_strcmp(char const *s1, char const *s2)
{
	int	i = 0;
	int	ret = 0;
	char	*tmps1;
	char	*tmps2;

	tmps1 = my_strlowcase(s1);
	tmps2 = my_strlowcase(s2);
	while (tmps1[i] != '\0' && tmps2[i] != '\0'){
		if (tmps1[i] == tmps2[i]){
			i++;
		} else if (tmps1[i] < tmps2[i]){
			ret = -1;
			break;
		} else {
			ret = 1;
			break;
		}
	}
	free(tmps1);
	free(tmps2);
	return (ret);
}
