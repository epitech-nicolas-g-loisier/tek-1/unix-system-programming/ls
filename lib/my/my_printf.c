/*
** EPITECH PROJECT, 2017
** my_printf
** File description:
** personal printf
*/

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "my.h"

int	check_flags(char flag)
{
	int	i = 0;
	char	*test = "csdibSouxXp";

	while (test[i] != 0){
		if (flag == test[i])
			return (i);
		i++;
	}
	return (84);
}

int	test(char* str, va_list ap)
{
	int	function;
	int	(*function_list[11])(va_list);

	function_list[0] = *my_putchar_p;
	function_list[1] = *my_putstr_p;
	function_list[2] = *my_putnbr_p;
	function_list[3] = *my_putnbr_p;
	function_list[4] = *bin_flag;
	function_list[5] = *special_flag;
	function_list[6] = *oct_flag;
	function_list[7] = *dec_flag;
	function_list[8] = *hexa_low_flag;
	function_list[9] = *hexa_up_flag;
	function_list[10] = *put_addres;
	function = check_flags(str[1]);
	if (function == 84)
		my_putchar('%');
	function_list[function](ap);
	return (1);
}

int	my_printf(char *str, ...)
{
	va_list	ap;
	int	i = 0;
	int	tmp;

	va_start(ap, str);
	while (str[i] != 0){
		if (str[i] == '%' && (str[i + 1] == '%' || str[i + 1] == 0)){
			my_putchar('%');
			i++;
		} else if (str[i] == '%'){
			tmp = test(&str[i], ap);
			if (tmp != 84)
				i += tmp;
		} else
			my_putchar(str[i]);
		i++;
	}
	va_end(ap);
	return (0);
}
