/*
** EPITECH PROJECT, 2017
** emacqs
** File description:
** my_revstr.c
*/

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include "my.h"

char	*my_revstr(char *str)
{
	int	count = 0;
	int	len = my_strlen(str) - 1;
	char	*new_str = malloc(sizeof(char) * (len + 2));

	while (len != 0){
		new_str[count] = str[len];
		len--;
		count++;
	}
	new_str[count] = 0;
	return (new_str);
}
