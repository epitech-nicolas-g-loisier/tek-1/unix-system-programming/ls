/*
** EPITECH PROJECT, 2017
** my_strlowcase
** File description:
** strlowcase
*/

#include <stdlib.h>
#include "my.h"

char	*my_strlowcase(char const *str)
{
	int	i = 0;
	char	*new_str = my_strdup(str);

	while (new_str[i] != '\0'){
		if (new_str[i] >= 'A' && new_str[i] <= 'Z'){
			new_str[i] = new_str[i] + 32;
		}
		i++;
	}
	return (new_str);
}
