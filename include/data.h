/*
** EPITECH PROJECT, 2017
** My ls
** File description:
** structure for data of ls
*/

#include <sys/stat.h>
#include <sys/types.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <dirent.h>
#include <pwd.h>
#include <grp.h>
#include <errno.h>
#include <time.h>
#include "my.h"

#ifndef DATA_H_
#define DATA_H_

typedef struct data
{
	int	block_size;
	char	*name;
	char	*permissions;
	char	*link;
	char	*owner;
	char	*group;
	char	*size;
	char	*date;
	char	*year;
	long	time;
	char	*path;
	char	*name2;
	struct	data	*next;
	struct	data	*prev;
} data_t;

#endif /* DATA_H_ */

#ifndef LS_PROTOTYPES_
#define LS_PROTOTYPES_

/* check_space.c */
void	check_space(data_t*);
void	replace_data(data_t*, int*);
char	*full_space(int, char*);

/* choose_option.c */
void	choose_name_display_option(data_t*, char*);
void	choose_full_display_option(data_t*, char*);

/* display_list.c */
char	*find_name(char*);
int	rev_display_list_name(data_t*);
int	rev_display_list_data(data_t*);
int	display_list_name(data_t*);
int	display_list_data(data_t*);

/* display_texte.c */
void    display_full_data(data_t*);
void    display_name(data_t*);

/* full_struct.c */
void	full_name(char*, char*);
void	full_data(char*, char*);
void	full_list(char*, char*);

/* get_data.c */
char	*get_uid(struct stat);
char	*get_gid(struct stat);
void	find_date(char*, data_t*);
char	*find_path(char*, char*);

/* get_permissions.c */
char	*get_permissions(struct stat);
char	*get_user_perm(struct stat, char*);
char	*get_grp_perm(struct stat, char*);
char	*get_other_perm(struct stat, char*);

/* manage_dir.c */
char	**sort_dir(char**);
int	check_if_exist(char*);
char	**exit_file(char**, char*);
void	choose_display_dir(char**, char*);
void	choose_dir(char**, char*);

/* manage_file.c */
void	add_data_file(char*, data_t*);
void	add_name_file(char*, data_t*);
void    display_info_file(char**, char*);

/* memory_tab.c */
void	free_tab_char(char**);
char	**malloc_tab_char(char**);

/* sort_list.c */
void	swap_data(data_t*, data_t*);
void	ascii_order(data_t*);
void	chrono_order(data_t*);

/* sort_option.c */
char	*check_option(char*, char);
char	*add_option(char*, char*);

/* useful_function.c */
data_t*	creat_root(void);
int	remove_name_list(data_t*);
int	remove_full_list(data_t*);
void	add_to_list_name(data_t*, struct dirent*, char*);
void	add_to_list_data(data_t*, struct dirent*, char*);

#endif /* LS_PROTOTYPES_ */
